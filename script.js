/*
 * Copyright (C) 2019 Evgeny Kapun.
 * This game is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version. This game is distributed without any warranty.
 */

"use strict";

let game = document.getElementById("game"), cross = document.getElementById("cross");
let w = 8, h = 8, dif = 1, nc, ed;
let sc1, sc2, se, dMin, dMax, stack, dCell, dEdge, clues = [];
let uiC, uiE, uiCE = [], uiEE = [], uiA, uiMCX, uiMCY, uiMEI, uiMEX1, uiMEY1, uiMEX2, uiMEY2, remains;

function randBelow(n) {
  return Math.floor(Math.random() * n);
}

function updateSize() {
  nc = w * h;
  let ne = (2 * h + 1) * w;
  ed = nc + w;
  sc1 = new Uint8Array(nc);
  sc2 = new Uint8Array(nc);
  se = new Uint8Array(ne);
  dMin = new Uint8Array(nc);
  dMax = new Uint8Array(nc);
  stack = new Uint32Array(nc);
  dCell = Int32Array.of(w, 1, -w, -1);
  dEdge = Int32Array.of(ed, 1, nc, 0);
  clues.length = nc;
  uiC = new Uint8Array(nc);
  uiE = new Uint8Array(ne);
  uiCE.length = nc;
  uiEE.length = ne;
}

function newGame() {
  if (h > 2 || w > 2) {
    done: while (true) {
      sc1.fill(0);
      let cnt = nc;
      while (true) {
        --cnt;
        let bestRes = 0, bestIds = [];
        for (let i = 0; i < nc; i++) {
          if (!sc1[i]) {
            sc1[i] = 1;
            let res = check(cnt);
            if (bestRes < res) {
              bestRes = res;
              bestIds.length = 1;
              bestIds[0] = i;
            } else if (bestRes == res) {
              bestIds.push(i);
            }
            sc1[i] = 0;
          }
        }
        if (bestIds.length > 0) {
          sc1[bestIds[randBelow(bestIds.length)]] = 1;
        } else {
          --cnt;
          for (let i = 0; i < nc; i++) {
            if (!sc1[i]) {
              sc1[i] = 1;
              let js = [];
              if (i % w < w - 1) {
                js.push(i + 1);
              }
              if (i < nc - w) {
                js.push(i + w);
              }
              for (let j of js) {
                if (!sc1[j]) {
                  sc1[j] = 1;
                  let res = check(cnt);
                  if (bestRes < res) {
                    bestRes = res;
                    bestIds.length = 1;
                    bestIds[0] = {i, j};
                  } else if (bestRes == res) {
                    bestIds.push({i, j});
                  }
                  sc1[j] = 0;
                }
              }
              sc1[i] = 0;
            }
          }
          if (bestIds.length > 0) {
            let {i, j} = bestIds[randBelow(bestIds.length)];
            sc1[i] = 1;
            sc1[j] = 1;
          } else {
            break;
          }
        }
        if (bestRes == cnt) {
          for (let i = 0; i < nc; i++) {
            clues[i] = sc1[i] ? {n: 0} : null;
          }
          for (let it = 0; it < 16; it++) {
            for (let i = 0; i < nc; i++) {
              if (clues[i]) {
                clues[i].d = randBelow(4);
              }
            }
            if (check2()) {
              break done;
            }
          }
        }
      }
    }
    let order = [];
    for (let i = 0; i < nc; i++) {
      if (clues[i]) {
        order.push(i);
      }
    }
    while (order.length > 0) {
      let cpos = randBelow(order.length), cur = order[cpos], curX = cur % w;
      if (cpos < order.length - 1) {
        order[cpos] = order.pop();
      } else {
        order.pop();
      }
      if (!((curX > 0 && clues[cur - 1] == 0) ||
        (curX < w - 1 && clues[cur + 1] == 0) ||
        (cur >= w && clues[cur - w] == 0) ||
        (cur < nc - w && clues[cur + w] == 0))) {
        let t = clues[cur];
        clues[cur] = 0;
        fix();
        if (!check2()) {
          clues[cur] = t;
        }
      }
    }
    fix();
  } else {
    clues.fill(null);
  }
  check2();
  while (cross.nextSibling) {
    cross.nextSibling.remove();
  }
  game.width.baseVal.value = w * 68 + 4;
  game.height.baseVal.value = h * 68 + 4;
  uiC.fill(0);
  uiE.fill(0);
  uiCE.fill(null);
  uiEE.fill(null);
  remains = nc;
  for (let y = 0; y < h; y++) {
    for (let x = 0; x < w; x++) {
      let cur = w * y + x, clue = clues[cur], cell = document.createElementNS("http://www.w3.org/2000/svg", "rect");
      cell.className.baseVal = clue ? "clue" : "cell";
      cell.width.baseVal.value = 64;
      cell.height.baseVal.value = 64;
      cell.x.baseVal.value = x * 68 + 4;
      cell.y.baseVal.value = y * 68 + 4;
      game.appendChild(cell);
      if (clue) {
        let arrow = document.createElementNS("http://www.w3.org/2000/svg", "use");
        arrow.href.baseVal = "#arrow";
        let tr = game.createSVGTransform();
        tr.setTranslate(x * 68 + 36, y * 68 + 36);
        arrow.transform.baseVal.initialize(tr);
        tr = game.createSVGTransform();
        tr.setRotate(-90 * clue.d, 0, 0);
        arrow.transform.baseVal.appendItem(tr);
        game.appendChild(arrow);
        let text = document.createElementNS("http://www.w3.org/2000/svg", "text");
        text.className.baseVal = "number";
        let xl = game.createSVGLength();
        xl.value = x * 68 + 36;
        text.x.baseVal.initialize(xl);
        let yl = game.createSVGLength();
        yl.value = y * 68 + 36 + (clue.d == 0 ? -6 : clue.d == 2 ? 6 : 0);
        text.y.baseVal.initialize(yl);
        text.textContent = clue.n;
        game.appendChild(text);
        uiE[cur + ed] = uiE[cur + nc] = uiE[cur + 1] = uiE[cur] = uiC[cur] = 3;
        --remains;
      }
    }
  }
  uiA = 0;
  uiMEY2 = uiMEX2 = uiMEY1 = uiMEX1 = uiMEI = uiMCY = uiMCX = -1;
}

function check(cnt) {
  se.fill(0);
  dMin.fill(0);
  dMax.fill(0);
  for (let i = 0; i < nc; i += w) {
    se[i] = 2;
    for (let j = i + 1; j < i + w; j++) {
      if (sc1[j - 1] == 0 && sc1[j] == 0) {
        ++dMax[j - 1];
        ++dMax[j];
      } else {
        se[j] = 2;
      }
    }
  }
  for (let i = 0; i < w; i++) {
    se[2 * nc + i] = se[nc + i] = 2;
  }
  for (let i = 0; i < nc - w; i++) {
    if (sc1[i] == 0 && sc1[i + w] == 0) {
      ++dMax[i];
      ++dMax[i + w];
    } else {
      se[i + ed] = 2;
    }
  }
  let stackSize = 0;
  for (let i = 0; i < nc; i++) {
    if (sc1[i] == 0) {
      if (dMax[i] <= 2) {
        if (dMax[i] < 2) {
          return -1;
        }
        stack[stackSize++] = i;
      }
    }
  }
  function setEdgeOff(i, j) {
    se[i] = 2;
    let deg = --dMax[j];
    if (deg <= 2) {
      if (deg < 2) {
        return true;
      }
      if (dMin[j] != 2) {
        stack[stackSize++] = j;
      }
    }
    return false;
  }
  function setEdgeOn(i, j, k) {
    ++dMin[i];
    se[j] = 1;
    let deg = ++dMin[k];
    if (deg >= 2) {
      if (deg > 2) {
        return true;
      }
      if (dMax[k] != 2) {
        stack[stackSize++] = k;
      }
    }
    return false;
  }
  while (true) {
    while (stackSize) {
      let cur = stack[--stackSize];
      if (dMax[cur] == 2) {
        if ((se[cur] == 0 && setEdgeOn(cur, cur, cur - 1)) ||
          (se[cur + 1] == 0 && setEdgeOn(cur, cur + 1, cur + 1)) ||
          (se[cur + nc] == 0 && setEdgeOn(cur, cur + nc, cur - w)) ||
          (se[cur + ed] == 0 && setEdgeOn(cur, cur + ed, cur + w))) {
          return -1;
        }
      } else {
        if ((se[cur] == 0 && setEdgeOff(cur, cur - 1)) ||
          (se[cur + 1] == 0 && setEdgeOff(cur + 1, cur + 1)) ||
          (se[cur + nc] == 0 && setEdgeOff(cur + nc, cur - w)) ||
          (se[cur + ed] == 0 && setEdgeOff(cur + ed, cur + w))) {
          return -1;
        }
      }
    }
    let changed = false;
    for (let i = 0; i < nc; i++) {
      if (dMin[i] == 1) {
        let ci = i, d = 1;
        do {
          let ni;
          for (d = (d - 1) & 3;; d = (d + 1) & 3) {
            if (se[ci + dEdge[d]] == 1) {
              break;
            }
          }
          ci += dCell[d];
        } while (dMin[ci] != 1);
        for (let d = 0; d < 4; d++) {
          if (ci == i + dCell[d]) {
            if (se[i + dEdge[d]] == 0) {
              se[i + dEdge[d]] = 2;
              if (--dMax[i] == 2) {
                stack[stackSize++] = i;
              }
              if (--dMax[ci] == 2) {
                stack[stackSize++] = ci;
              }
              changed = true;
            }
            break;
          }
        }
      }
    }
    if (!changed) {
      break;
    }
  }
  stack.fill(0);
  let cc = 0;
  for (let i = 0; i < nc; i++) {
    if (dMin[i] == 1 && !stack[i]) {
      stack[i] = 1;
      let ci = i, d = 1;
      do {
        for (d = (d - 1) & 3;; d = (d + 1) & 3) {
          if (se[ci + dEdge[d]] == 1) {
            break;
          }
        }
        ci += dCell[d];
        stack[ci] = 1;
        ++cc;
      } while (dMin[ci] != 1);
    }
  }
  for (let i = 0; i < nc; i++) {
    if (!stack[i] && dMin[i] == 2) {
      let c = 0, ci = i, d = 1;
      do {
        for (d = (d - 1) & 3;; d = (d + 1) & 3) {
          if (se[ci + dEdge[d]] == 1) {
            break;
          }
        }
        ci += dCell[d];
        stack[ci] = 1;
        ++c;
      } while (ci != i);
      return c == cnt ? cnt : -1;
    }
  }
  return cc;
}

function rangeSize(i, d) {
  let pos, size;
  if (d & 1) {
    pos = i % w;
    size = w;
  } else {
    pos = (i / w) | 0;
    size = h;
  }
  return d & 2 ? pos : size - pos - 1;
}

function check2() {
  sc2.fill(0);
  se.fill(0);
  for (let i = 0; i < nc; i += w) {
    se[i] = 2;
  }
  for (let i = 0; i < w; i++) {
    se[2 * nc + i] = se[nc + i] = 2;
  }
  let nClues = 0, setPos = new Uint32Array(nc + 1);
  for (let i = 0; i < nc; i++) {
    let c = clues[i];
    if (c) {
      se[i + ed] = se[i + nc] = se[i + 1] = se[i] = sc2[i] = 2;
      let l = rangeSize(i, c.d), dj = dCell[c.d];
      for (let k = 0, j = i + dj; k < l; ++k, j += dj) {
        let cc = clues[j];
        if (!cc) {
          ++setPos[j];
        } else if (dif >= 1 && cc.d == c.d) {
          break;
        }
      }
      ++nClues;
    }
  }
  let nSets = 0;
  for (let i = 0; i < nc; i++) {
    let cur = setPos[i];
    setPos[i] = nSets;
    nSets += cur;
  }
  setPos[nc] = nSets;
  let clueStart = new Int32Array(nClues), clueLen = new Uint32Array(nClues),
    clueD = new Uint8Array(nClues), clueDCell = new Int32Array(nClues),
    clueOff = new Uint32Array(nClues), clueOn = new Uint32Array(nClues),
    setId = new Uint32Array(nSets), curClue = 0,
    clueStack = new Uint32Array(nClues), stackSize = 0, clueStackSize = 0;
  dMin.fill(0);
  dMax.fill(0);
  for (let i = 0; i < nc; i++) {
    let c = clues[i];
    if (c) {
      let cD = c.d, cLen = rangeSize(i, cD), cDCell = dCell[cD], cStart = i + cDCell, cOff = c.n, cOn = -cOff;
      for (let i = 0, j = cStart; i < cLen; i++, j += cDCell) {
        let cc = clues[j];
        if (!cc) {
          setId[setPos[j]++] = curClue;
          ++cOn;
        } else if (dif >= 1 && cc.d == cD) {
          cLen = i;
          cOff -= cc.n;
          cOn += cc.n;
          break;
        }
      }
      if ((cOff | cOn) < 0) {
        throw 0;
      }
      if ((cOn == 0) != (cOff == 0)) {
        clueStack[clueStackSize++] = curClue;
      }
      clueStart[curClue] = cStart;
      clueLen[curClue] = cLen;
      clueD[curClue] = cD;
      clueDCell[curClue] = cDCell;
      clueOff[curClue] = cOff;
      clueOn[curClue] = cOn;
      ++curClue;
    } else {
      let deg = (se[i] == 0) + (se[i+1] == 0) + (se[i+nc] == 0) + (se[i+ed] == 0);
      dMax[i] = deg;
      if (deg < 2) {
        stack[stackSize++] = i;
      }
    }
  }
  setPos.copyWithin(1, 0, nc - 1);
  setPos[0] = 0;
  function setEdgeOff(i, j) {
    se[i] = 2;
    let s = sc2[j], deg = --dMax[j];
    if (s == 0) {
      if (deg == 1) {
        stack[stackSize++] = j;
      }
    } else if (deg <= 2) {
      if (deg < 2) {
        return true;
      }
      if (dMin[j] != 2) {
        stack[stackSize++] = j;
      }
    }
    return false;
  }
  function setEdgeOn(i, j, k) {
    ++dMin[i];
    se[j] = 1;
    let s = sc2[k], deg = ++dMin[k];
    if (s == 0) {
      if (setCellOn(k)) {
        return true;
      }
    } else if (deg >= 2) {
      if (deg > 2) {
        return true;
      }
      if (dMax[k] != 2) {
        stack[stackSize++] = k;
      }
    }
    return false;
  }
  function setCellOff(i) {
    sc2[i] = 2;
    for (let j = setPos[i], l = setPos[i + 1]; j < l; j++) {
      let s = setId[j], v = --clueOff[s];
      if (v <= 0) {
        if (v < 0) {
          return true;
        }
        if (clueOn[s] != 0) {
          clueStack[clueStackSize++] = s;
        }
      }
    }
    if ((se[i] == 0 && (setEdgeOff(i, i - 1) || (sc2[i - 1] == 0 && setCellOn(i - 1)))) ||
      (se[i + 1] == 0 && (setEdgeOff(i + 1, i + 1) || (sc2[i + 1] == 0 && setCellOn(i + 1)))) ||
      (se[i + nc] == 0 && (setEdgeOff(i + nc, i - w) || (sc2[i - w] == 0 && setCellOn(i - w)))) ||
      (se[i + ed] == 0 && (setEdgeOff(i + ed, i + w) || (sc2[i + w] == 0 && setCellOn(i + w))))) {
      return true;
    }
    return false;
  }
  function setCellOn(i) {
    sc2[i] = 1;
    for (let j = setPos[i], l = setPos[i + 1]; j < l; j++) {
      let s = setId[j], v = --clueOn[s];
      if (v <= 0) {
        if (v < 0) {
          return true;
        }
        if (clueOff[s] != 0) {
          clueStack[clueStackSize++] = s;
        }
      }
    }
    let deg = dMax[i];
    if (deg <= 2) {
      if (deg < 2) {
        return true;
      }
      stack[stackSize++] = i;
    }
    return false;
  }
  function checkClose(c) {
    for (let i = 0; i < nClues; i++) {
      if (clueOn[i] != 0) {
        return true;
      }
    }
    for (let i = 0; i < nc; i++) {
      if (sc2[i] == 0 && (
        (se[i + 1] == 0 && sc2[i + 1] == 0) ||
        (se[i + ed] == 0 && sc2[i + w] == 0))) {
        return true;
      }
    }
    let ones = 0;
    for (let i = 0; i < nc; i++) {
      if (sc2[i] == 1) {
        ++ones;
      }
    }
    return ones != c;
  }
  let seen = new Uint8Array(nc);
  all: while (true) {
    while (stackSize > 0) {
      let cur = stack[--stackSize];
      switch (sc2[cur]) {
      case 0:
        if (setCellOff(cur)) {
          throw 0;
        }
        break;
      case 1:
        if (dMax[cur] == 2) {
          if ((se[cur] == 0 && setEdgeOn(cur, cur, cur - 1)) ||
            (se[cur + 1] == 0 && setEdgeOn(cur, cur + 1, cur + 1)) ||
            (se[cur + nc] == 0 && setEdgeOn(cur, cur + nc, cur - w)) ||
            (se[cur + ed] == 0 && setEdgeOn(cur, cur + ed, cur + w))) {
            throw 0;
          }
        } else {
          if ((se[cur] == 0 && setEdgeOff(cur, cur - 1)) ||
            (se[cur + 1] == 0 && setEdgeOff(cur + 1, cur + 1)) ||
            (se[cur + nc] == 0 && setEdgeOff(cur + nc, cur - w)) ||
            (se[cur + ed] == 0 && setEdgeOff(cur + ed, cur + w))) {
            throw 0;
          }
        }
      }
    }
    if (clueStackSize > 0) {
      let cur = clueStack[--clueStackSize];
      if (clueOff[cur] == 0) {
        for (let i = 0, j = clueStart[cur], l = clueLen[cur], dj = clueDCell[cur]; i < l; i++, j += dj) {
          if (sc2[j] == 0 && setCellOn(j)) {
            throw 0;
          }
        }
      } else {
        for (let i = 0, j = clueStart[cur], l = clueLen[cur], dj = clueDCell[cur]; i < l; i++, j += dj) {
          if (sc2[j] == 0 && setCellOff(j)) {
            throw 0;
          }
        }
      }
      continue;
    }
    if (dif >= 1) {
      for (let i = 0; i < nc; i++) {
        if (sc2[i] == 0 && dMax[i] == 2) {
          for (let d = 0; d < 4; d++) {
            if (se[i + dEdge[d]] == 0 && sc2[i + dCell[d]] == 0) {
              if (setCellOn(i + dCell[d])) {
                throw 0;
              }
              continue all;
            }
          }
        }
      }
      for (let i1 = 0; i1 < nc; i1++) {
        if (dMin[i1] == 1) {
          for (let d1 = 0; d1 < 4; d1++) {
            let dc1 = dCell[d1], de1 = dEdge[d1], d2 = (d1 + 1) & 3, dc2 = dCell[d2], de2 = dEdge[d2];
            let i2 = i1 + dc1, i3 = i1 + dc2, i4 = i2 + dc2;
            if (se[i1 + de1] == 0 && se[i1 + de2] == 0 && dMax[i2] == 3 && dMax[i3] == 3 &&
              se[i2 + de2] == 0 && se[i3 + de1] == 0 && sc2[i4] == 0) {
              if (setCellOn(i4)) {
                throw 0;
              }
              continue all;
            }
          }
        }
      }
    }
    for (let cur = 0; cur < nClues; cur++) {
      let co = clueOff[cur];
      if (co != 0) {
        let cStart = clueStart[cur], cLen = clueLen[cur], cD = clueD[cur],
          cDCell = clueDCell[cur], cDEdge = dEdge[cD], next = 0;
        for (let i = 0, j = cStart; i < cLen; i++, j += cDCell) {
          if (sc2[j] == 0 && i >= next) {
            seen[i] = 1;
            --co;
            let skip = 2;
            if (dif >= 1 && i + 2 < cLen) {
              let j2 = j + cDCell, j3 = j2 + cDCell;
              if (se[j + cDEdge] == 0 && se[j2 + cDEdge] == 0 && dMax[j2] <= 3) {
                skip = 3;
              } else if (sc2[j2] <= 1) {
                for (let dd = 1; dd <= 3; dd += 2) {
                  let d = (cD + dd) & 3, dc = dCell[d], de = dEdge[d];
                  if (se[j2 + de] <= 1 &&
                    sc2[j + dc] <= 1 && dMax[j + dc] - (se[j + de] <= 1) <= 2 &&
                    sc2[j3 + dc] <= 1 && dMax[j3 + dc] - (se[j3 + de] <= 1) <= 2) {
                    skip = 3;
                    break;
                  }
                }
              }
            }
            next = i + skip;
          } else {
            seen[i] = 0;
          }
        }
        if (co >= 0) {
          if (co > 0) {
            throw 0;
          }
          next = cLen - 1;
          let changed = false;
          for (let i = cLen - 1, j = cStart + i * cDCell; i >= 0; i--, j -= cDCell) {
            if (sc2[j] == 0 && i <= next) {
              if (seen[i]) {
                if (setCellOff(j)) {
                  throw 0;
                }
                changed = true;
              }
              let skip = 2;
              if (dif >= 1 && i - 2 >= 0) {
                let j2 = j - cDCell, j3 = j2 - cDCell;
                if (se[j2 + cDEdge] == 0 && se[j3 + cDEdge] == 0 && dMax[j2] <= 3) {
                  skip = 3;
                } else if (sc2[j2] <= 1) {
                  for (let dd = 1; dd <= 3; dd += 2) {
                    let d = (cD + dd) & 3, dc = dCell[d], de = dEdge[d];
                    if (se[j2 + de] <= 1 &&
                      sc2[j + dc] <= 1 && dMax[j + dc] - (se[j + de] <= 1) <= 2 &&
                      sc2[j3 + dc] <= 1 && dMax[j3 + dc] - (se[j3 + de] <= 1) <= 2) {
                      skip = 3;
                      break;
                    }
                  }
                }
              }
              next = i - skip;
            }
          }
          if (changed) {
            continue all;
          }
        }
      }
    }
    seen.fill(0);
    for (let i = 0; i < nc; i++) {
      if (dMin[i] == 1) {
        seen[i] = 1;
        let c = 1, ci = i, pi = -1;
        do {
          let ni = se[ci] == 1 && pi != ci - 1 ? ci - 1 :
            se[ci + 1] == 1 && pi != ci + 1 ? ci + 1 :
            se[ci + nc] == 1 && pi != ci - w ? ci - w : ci + w;
          seen[ni] = 1;
          ++c;
          pi = ci;
          ci = ni;
        } while (dMin[ci] != 1);
        for (let d = 0; d < 4; d++) {
          if (ci == i + dCell[d] && se[i + dEdge[d]] == 0 && checkClose(c)) {
            let deg = --dMax[i];
            if (deg <= 2) {
              if (deg < 2) {
                throw 0;
              }
              if (dMin[i] != 2) {
                stack[stackSize++] = i;
              }
            }
            if (setEdgeOff(i + dEdge[d], ci)) {
              throw 0;
            }
            continue all;
          }
        }
      }
    }
    for (let i = 0; i < nc; i++) {
      if (dMin[i] == 2 && !seen[i]) {
        let c = 0, ci = i, pi = -1;
        do {
          let ni = se[ci] == 1 && pi != ci - 1 ? ci - 1 :
            se[ci + 1] == 1 && pi != ci + 1 ? ci + 1 :
            se[ci + nc] == 1 && pi != ci - w ? ci - w : ci + w;
          seen[ni] = 1;
          ++c;
          pi = ci;
          ci = ni;
        } while (ci != i);
        if (checkClose(c)) {
          throw 0;
        }
      }
    }
    let zeros = 0, ones = 0;
    for (let i = 0; i < nc; i++) {
      switch (sc2[i]) {
      case 0:
        ++zeros;
        break;
      case 1:
        ++ones;
        break;
      }
    }
    if (zeros + ones <= 4 && zeros != 0) {
      for (let i = 0; i < nc; i++) {
        if (sc2[i] == 0 && setCellOn(i)) {
          throw 0;
        }
      }
      continue;
    }
    break;
  }
  for (let i = 0; i < nc; i++) {
    if (sc2[i] == 0 || (sc2[i] == 1 && dMin[i] != 2)) {
      return false;
    }
  }
  return true;
}

function fix() {
  for (let c = 0; c < nc; c++) {
    if (clues[c]) {
      let d = clues[c].d, l = rangeSize(c, d), dj = dCell[d], n = 0;
      for (let i = 0, j = c + dj; i < l; i++, j += dj) {
        if (clues[j] == 0) {
          ++n;
        }
      }
      clues[c].n = n;
    }
  }
}

game.onmousedown = function(e) {
  uiA = 0;
  uiMEY2 = uiMEX2 = uiMEY1 = uiMEX1 = uiMEI = uiMCY = uiMCX = -1;
  let b1 = e.button, b2 = e.buttons, a;
  if (b1 == 0 && b2 == 1) {
    a = 1;
  } else if (b1 == 2 && b2 == 2) {
    a = 2;
  } else {
    return;
  }
  let cx = e.offsetX, cy = e.offsetY,
    x = Math.floor((cx - 20) / 68), y = Math.floor((cy - 20) / 68);
  if (cx - 68 * x < 52 && cy - 68 * y < 52) {
    if (x >= 0 && x < w && y >= 0 && y < h) {
      e.preventDefault();
      uiA = a;
      uiMCX = x;
      uiMCY = y;
    }
  } else {
    let s = Math.floor((cx + cy - 4) / 68), d = Math.floor((cy - cx) / 68);
    if ((s ^ d) & 1) {
      let x = (s - d) >> 1, y = ((s + d) >> 1) + 1;
      if (x >= 0 && x < w && y > 0 && y < h) {
        e.preventDefault();
        uiA = a;
        uiMEI = nc + w * y + x;
        uiMEX2 = uiMEX1 = x;
        uiMEY1 = y - 1;
        uiMEY2 = y;
      }
    } else {
      let x = (s - d) >> 1, y = (s + d) >> 1;
      if (x > 0 && x < w && y >= 0 && y < h) {
        e.preventDefault();
        uiA = a;
        uiMEI = w * y + x;
        uiMEX1 = x - 1;
        uiMEX2 = x;
        uiMEY2 = uiMEY1 = y;
      }
    }
  }
};

game.onmouseup = function(e) {
  if (uiA != 0 && e.button == (uiA == 1 ? 0 : 2) && e.buttons == 0) {
    let cx = e.offsetX, cy = e.offsetY;
    if (uiMCX >= 0) {
      let rx = cx - 68 * uiMCX, ry = cy - 68 * uiMCY;
      if (rx >= 20 && rx < 52 && ry >= 20 && ry < 52) {
        let cur = w * uiMCY + uiMCX;
        switch (uiC[cur]) {
        case 0:
          if (uiA == 1) {
            if (uiE[cur] != 1 && uiE[cur + 1] != 1 && uiE[cur + nc] != 1 && uiE[cur + ed] != 1) {
              uiC[cur] = 2;
              uiCE[cur] = document.createElementNS("http://www.w3.org/2000/svg", "rect");
              uiCE[cur].x.baseVal.value = 68 * uiMCX + 4;
              uiCE[cur].y.baseVal.value = 68 * uiMCY + 4;
              uiCE[cur].width.baseVal.value = 64;
              uiCE[cur].height.baseVal.value = 64;
              game.appendChild(uiCE[cur]);
              if (sc2[cur] == 2) {
                --remains;
              } else {
                ++remains;
              }
            }
          } else {
            uiC[cur] = 1;
            uiCE[cur] = document.createElementNS("http://www.w3.org/2000/svg", "circle");
            uiCE[cur].cx.baseVal.value = 68 * uiMCX + 36;
            uiCE[cur].cy.baseVal.value = 68 * uiMCY + 36;
            uiCE[cur].r.baseVal.value = 4;
            game.appendChild(uiCE[cur]);
          }
          break;
        case 2:
          if (sc2[cur] == 2) {
            ++remains;
          } else {
            --remains;
          }
        case 1:
          uiC[cur] = 0;
          uiCE[cur].remove();
          uiCE[cur] = null;
        }
      }
    } else if (uiMEX1 >= 0) {
      let rs = cx + cy - 34 * (uiMEX1 + uiMEY1 + uiMEX2 + uiMEY2),
        rd = cy - cx - 34 * (uiMEY1 - uiMEX1 + uiMEY2 - uiMEX2);
      if (rs >= 38 && rs < 106 && rd >= -34 && rd < 34) {
        switch (uiE[uiMEI]) {
        case 0:
          if (uiA == 1) {
            if (uiC[w * uiMEY1 + uiMEX1] <= 1 && uiC[w * uiMEY2 + uiMEX2] <= 1) {
              uiE[uiMEI] = 1;
              uiEE[uiMEI] = document.createElementNS("http://www.w3.org/2000/svg", "line");
              uiEE[uiMEI].x1.baseVal.value = 68 * uiMEX1 + 36;
              uiEE[uiMEI].y1.baseVal.value = 68 * uiMEY1 + 36;
              uiEE[uiMEI].x2.baseVal.value = 68 * uiMEX2 + 36;
              uiEE[uiMEI].y2.baseVal.value = 68 * uiMEY2 + 36;
              game.appendChild(uiEE[uiMEI]);
              if (se[uiMEI] == 1) {
                --remains;
              } else {
                ++remains;
              }
            }
          } else {
            uiE[uiMEI] = 2;
            uiEE[uiMEI] = document.createElementNS("http://www.w3.org/2000/svg", "use");
            uiEE[uiMEI].href.baseVal = "#cross";
            uiEE[uiMEI].x.baseVal.value = 34 * (uiMEX1 + uiMEX2) + 36;
            uiEE[uiMEI].y.baseVal.value = 34 * (uiMEY1 + uiMEY2) + 36;
            game.appendChild(uiEE[uiMEI]);
          }
          break;
        case 1:
          if (se[uiMEI] == 1) {
            ++remains;
          } else {
            --remains;
          }
        case 2:
          uiE[uiMEI] = 0;
          uiEE[uiMEI].remove();
          uiEE[uiMEI] = null;
        }
      }
    }
    if (remains == 0) {
      document.body.style.overflow = "hidden";
      document.getElementById("solved-bg").style.display = "flex";
      remains = 1;
    }
  }
  uiA = 0;
  uiMEY2 = uiMEX2 = uiMEY1 = uiMEX1 = uiMEI = uiMCY = uiMCX = -1;
};

game.oncontextmenu = function(e) {
  e.preventDefault();
};

document.getElementById("newgame").onclick = newGame;

document.getElementById("options").onclick = function() {
  document.body.style.overflow = "hidden";
  document.getElementById("options-bg").style.display = "flex";
  document.getElementById("width").valueAsNumber = w;
  document.getElementById("height").valueAsNumber = h;
  document.getElementById("difficulty").selectedIndex = dif;
  document.getElementById("width").focus();
};

document.getElementById("options-bg").onsubmit = function(e) {
  e.preventDefault();
  document.getElementById("options-bg").style.display = null;
  document.body.style.overflow = null;
  w = document.getElementById("width").valueAsNumber;
  h = document.getElementById("height").valueAsNumber;
  dif = document.getElementById("difficulty").selectedIndex;
  updateSize();
  newGame();
};

document.getElementById("options-cancel").onclick = function() {
  document.getElementById("options-bg").style.display = null;
  document.body.style.overflow = null;
};

document.getElementById("solved-ok").onclick = function() {
  document.getElementById("solved-bg").style.display = null;
  document.body.style.overflow = null;
};

onkeydown = function(e) {
  if (e.key == "Escape") {
    if (document.getElementById("options-bg").style.display == "flex") {
      document.getElementById("options-bg").style.display = null;
      document.body.style.overflow = null;
    } else if (document.getElementById("solved-bg").style.display == "flex") {
      document.getElementById("solved-bg").style.display = null;
      document.body.style.overflow = null;
    }
  }
};

updateSize();
newGame();