This is an implementation of Yajilin puzzle game.


Running the game
================

Just open game.html in a web browser.


Game rules
==========

There is a rectangular grid of squares. Some squares contain clues: each clue has a number and an arrow. The objective is to color some squares black and to draw a loop through the remaining squares such that these conditions are satisfied:

* No square with a clue is colored black.
* No two black squares share a side.
* The loop passes exactly once through each square that is not black and doesn't contain a clue, and doesn't pass through any other square.
* Each pair of squares that are adjacent in the loop shares a side.
* For each clue, the number in the clue matches the number of black squares in the direction pointed by the arrow.


How to play
===========

Use left mouse button to mark edges and black squares. Use right mouse button to mark places where there is certainly no edge and squares which the loop certainly passes through (that is, squares that are certainly not black). Once the solution is found, a message will be shown.

Use the options dialog to change the size of the grid, as well as the difficulty.


License
=======

This game is copyright (C) 2019 Evgeny Kapun.

This game is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This game is distributed without any warranty.

The text of the GNU General Public License can be found in a file named COPYING.